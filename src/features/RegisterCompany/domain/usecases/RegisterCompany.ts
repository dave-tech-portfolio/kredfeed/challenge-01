import { CompanyRecord } from "../entities/models/Company";
import CompaniesAPI from "../entities/services/CompaniesAPI";

export default class CompaniesUseCases {
    constructor(private companiesAPI: CompaniesAPI) { }

    async register(comapnyRecord: CompanyRecord) {
        const id = await this.companiesAPI.register(comapnyRecord);
    }
}