
export type Address = {
    street: string;
    extNumber: number;
    intNumber: number;
    zipcode: string;
    neighborhood: string;
    municipality: string;
    city: string;
    state: string;
    country: string;
    recidenceProof: string;
}

export type Company = {
    name: string;
    email: string;
    incorporationDate: Date;
    address: Address;
}

export type AuthorizedSingatory = {
    name: string;
    email: string;
    nationality: string;
    birthday: Date
    address: Address;
    idCard: string;
}

export type CompanyRecord = {
    company: Company;
    authSignatory: AuthorizedSingatory;
}

export function createCompanyRecord() {
    return {
        company: {
            name: '',
            email: '',
            incorporationDate: new Date(),
            address: {
                street: '',
                extNumber: 0,
                intNumber: 0,
                zipcode: '',
                neighborhood: '',
                municipality: '',
                state: '',
                city: '',
                country: '',
                recidenceProof: '',
            },
        },
        authSignatory: {
            name: '',
            email: '',
            birthday: new Date(),
            nationality: '',
            idCard: '',
            address: {
                street: '',
                extNumber: 0,
                intNumber: 0,
                zipcode: '',
                neighborhood: '',
                municipality: '',
                state: '',
                city: '',
                country: '',
                recidenceProof: '',
            }
        }
    }
}