import { CompanyRecord } from "../models/Company";

export default interface CompaniesAPI {
    register(companyRecord: CompanyRecord): Promise<number>;
}