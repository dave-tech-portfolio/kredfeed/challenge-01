import { CompanyRecord } from "../../domain/entities/models/Company";
import CompaniesAPI from "../../domain/entities/services/CompaniesAPI";

export default class NewAPI implements CompaniesAPI {
    async register(companyRecord: CompanyRecord): Promise<number> {
        await this.delay(2000);
        return Math.floor(Math.random() * 1000);
    }

    delay(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

}