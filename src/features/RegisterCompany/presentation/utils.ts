import { CompanyRecord } from "../domain/entities/models/Company";

export const validateFormData = (formData: CompanyRecord) => {
    const company = formData.company;
    const authSignatory = formData.authSignatory;
    const today = new Date();
    const regex = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;

    if (
        !company.name ||
        !company.email || (!regex.test(company.email)) ||
        !company.incorporationDate || (company.incorporationDate > today) ||
        !company.address.recidenceProof ||
        !company.address.street ||
        !company.address.extNumber ||
        !company.address.zipcode ||
        !company.address.neighborhood ||
        !company.address.municipality ||
        !company.address.city ||
        !company.address.state ||
        !company.address.country ||
        !authSignatory.name ||
        !authSignatory.email || (!regex.test(authSignatory.email)) ||
        !authSignatory.birthday || (authSignatory.birthday > today) ||
        !authSignatory.nationality ||
        !authSignatory.address.recidenceProof ||
        !authSignatory.idCard ||
        !authSignatory.address.street ||
        !authSignatory.address.extNumber ||
        !authSignatory.address.zipcode ||
        !authSignatory.address.neighborhood ||
        !authSignatory.address.municipality ||
        !authSignatory.address.city ||
        !authSignatory.address.state ||
        !authSignatory.address.country
    ) {
        return false;
    }

    return true;
};
