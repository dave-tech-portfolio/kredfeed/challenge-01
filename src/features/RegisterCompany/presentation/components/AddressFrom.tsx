import styled from "styled-components";
import BrandedInput, { InputTypes } from "../../../../components/BrandedInput";

const StyledRowContainer = styled.div`
    display: flex;
    felx-direction: row;
    @media (max-width: 628px) {
        flex-direction: column;
    }
`;

export default function AddressForm({ addressChanged }: {
    addressChanged: React.ChangeEventHandler<HTMLInputElement> | undefined
}) {
    const template = <>
        <BrandedInput
            label="Calle o Avenida"
            type={InputTypes.TEXT}
            maxLength={50}
            required
            name="street"
            onChange={addressChanged}
        />
        <StyledRowContainer>
            <BrandedInput
                label="Número exterior"
                type={InputTypes.NUMBER}
                maxLength={50}
                required
                name="extNumber"
                onChange={addressChanged}
            />
            <BrandedInput
                label="Número exterior"
                type={InputTypes.NUMBER}
                maxLength={50}
                required
                name="intNumber"
                onChange={addressChanged}
            />
            <BrandedInput
                label="Código Postal"
                type={InputTypes.NUMBER}
                maxLength={50}
                required
                name="zipcode"
                onChange={addressChanged}
            />
        </StyledRowContainer>
        <BrandedInput
            label="Colonia"
            type={InputTypes.TEXT}
            maxLength={50}
            required
            name="neighborhood"
            onChange={addressChanged}
        />
        <BrandedInput
            label="Municipio"
            type={InputTypes.TEXT}
            maxLength={50}
            required
            name="municipality"
            onChange={addressChanged}
        />


        <StyledRowContainer>
            <BrandedInput
                label="Ciudad"
                type={InputTypes.TEXT}
                maxLength={50}
                required
                name="state"
                onChange={addressChanged}
            />
            <BrandedInput
                label="Estado"
                type={InputTypes.TEXT}
                maxLength={50}
                required
                name="city"
                onChange={addressChanged}
            />
            <BrandedInput
                label="País"
                type={InputTypes.TEXT}
                maxLength={50}
                required
                name="country"
                onChange={addressChanged}
            />
        </StyledRowContainer>
        <BrandedInput
            label="Comprobante de domicilio (documento)"
            type={InputTypes.FILE}
            required
            onChange={addressChanged}
            name="recidenceProof"
        />
    </>

    return template;
}