import BrandedInput, { InputTypes } from "../../../../components/BrandedInput";
import BrandedSection from "../../../../components/BrandedSection";
import AddressForm from "./AddressFrom";
import GeneralInfoForm from "./GeneralInfoForm";

export default function CompanyForm(
    { handleChange, handleAddressChange }:
        {
            handleChange: React.ChangeEventHandler<HTMLInputElement> | undefined,
            handleAddressChange: React.ChangeEventHandler<HTMLInputElement> | undefined
        }
) {
    const template = <>
        <BrandedSection title="Datos generales">
            <GeneralInfoForm infoChanged={handleChange} />
            <BrandedInput
                label="Fecha de constitución"
                type={InputTypes.DATE}
                required
                onChange={handleChange}
                name="incorporationDate"
                defaultValue={(new Date()).toISOString().split('T')[0]}
            />

            <AddressForm addressChanged={handleAddressChange} />
        </BrandedSection>
    </>

    return template;
}