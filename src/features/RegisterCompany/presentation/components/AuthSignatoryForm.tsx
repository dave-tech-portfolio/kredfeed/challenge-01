import BrandedInput, { InputTypes } from "../../../../components/BrandedInput";
import BrandedSection from "../../../../components/BrandedSection";
import AddressForm from "./AddressFrom";
import GeneralInfoForm from "./GeneralInfoForm";

export default function AuthSignatoryForm(
    { handleChange, handleAddressChange }:
        {
            handleChange: React.ChangeEventHandler<HTMLInputElement> | undefined,
            handleAddressChange: React.ChangeEventHandler<HTMLInputElement> | undefined
        }
) {
    const template = <>
        <BrandedSection title="Representante legal">
            <GeneralInfoForm infoChanged={handleChange} />

            <BrandedInput
                label="Fecha de nacimiento"
                type={InputTypes.DATE}
                required
                onChange={handleChange}
                name="birthday"
                defaultValue={(new Date()).toISOString().split('T')[0]}
            />

            <BrandedInput
                label="Nacionalidad"
                type={InputTypes.TEXT}
                required
                onChange={handleChange}
                name="nationality"
            />

            <AddressForm addressChanged={handleAddressChange} />


            <BrandedInput
                label="Identificación (documento)"
                type={InputTypes.FILE}
                required
                onChange={handleChange}
                name="idCard"
            />
        </BrandedSection>
    </>;

    return template;
}