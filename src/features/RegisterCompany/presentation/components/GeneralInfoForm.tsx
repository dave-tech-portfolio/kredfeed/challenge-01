import BrandedInput, { InputTypes } from "../../../../components/BrandedInput";

export default function GeneralInfoForm({ infoChanged }: {
    infoChanged: React.ChangeEventHandler<HTMLInputElement> | undefined
}) {
    const template = <>
        <BrandedInput
            label="Nombre"
            type={InputTypes.TEXT}
            maxLength={50}
            required
            onChange={infoChanged}
            name="name"

        />
        <BrandedInput
            label="Correo electrónico"
            type={InputTypes.EMAIL}
            maxLength={50}
            required
            onChange={infoChanged}
            name="email"
        />
    </>

    return template;
}