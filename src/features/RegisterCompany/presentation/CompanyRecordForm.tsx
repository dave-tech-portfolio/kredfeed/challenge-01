import { useState } from "react";
import styled from "styled-components";
import BrandedButton from "../../../components/BrandedButton";
import BrandedCircularProgressIndicator from "../../../components/BrandedCircularProgressIndicator";
import { CompanyRecord, createCompanyRecord } from "../domain/entities/models/Company";
import CompaniesUseCases from "../domain/usecases/RegisterCompany";
import NewAPI from "../infrastructure/services/MockApi";
import AuthSignatoryForm from "./components/AuthSignatoryForm";
import CompanyForm from "./components/CompanyForm";
import { validateFormData } from "./utils";


const companiesUseCases = new CompaniesUseCases(new NewAPI);

const StyledContainer = styled.div`
    display: flex;
    justify-content: flex-end;
    padding: 25px;
`;

const StyledIndicatorContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
`;

export default function CompanyRecordForm() {
    const [isProcessing, setProcessing] = useState(false);
    const [isValidForm, setValidForm] = useState(false);
    const [companyRecord, setCompanyRecord] = useState<CompanyRecord>(createCompanyRecord());

    const handleSubmit = () => {
        setProcessing(true);
        companiesUseCases.register(companyRecord)
            .then(() => setProcessing(false))
            .catch(() => console.log('Error'));
    }

    const validateAndSetState = (updatedRecord: CompanyRecord) => {
        const isValid = validateFormData(updatedRecord);
        setCompanyRecord(updatedRecord);
        setValidForm(isValid);
    }

    const handleCompanyAddressChange = (event: any) => {
        const { value, name } = event.target;
        const updatedRecord = {
            ...companyRecord,
            company: {
                ...companyRecord.company,
                address: {
                    ...companyRecord.company.address,
                    [name]: value,
                }
            }
        };
        validateAndSetState(updatedRecord);
    }

    const handleAuthSignatoryAddressChange = (event: any) => {
        const { value, name } = event.target;
        const updatedRecord = {
            ...companyRecord,
            authSignatory: {
                ...companyRecord.authSignatory,
                address: {
                    ...companyRecord.authSignatory.address,
                    [name]: value,
                }
            }
        };
        validateAndSetState(updatedRecord);
    }

    const handleCompanyChange = (event: any) => {
        const { value, name } = event.target;
        const updatedRecord = {
            ...companyRecord,
            company: {
                ...companyRecord.company,
                [name]: value,
            }
        };

        validateAndSetState(updatedRecord);
    }

    const handleAuthSignatoryChange = (event: any) => {
        const { value, name } = event.target;
        const updatedRecord = {
            ...companyRecord,
            authSignatory: {
                ...companyRecord.authSignatory,
                [name]: value,
            }
        };

        validateAndSetState(updatedRecord);
    }

    const form = <>
        <h2>Registro de Empresa</h2>

        <CompanyForm handleChange={handleCompanyChange} handleAddressChange={handleCompanyAddressChange} />

        <AuthSignatoryForm handleChange={handleAuthSignatoryChange} handleAddressChange={handleAuthSignatoryAddressChange} />

        <StyledContainer>
            <BrandedButton disabled={!isValidForm} onClick={handleSubmit}>Aceptar</BrandedButton>
        </StyledContainer>
    </>

    const progressIndicator = <StyledIndicatorContainer>
        <BrandedCircularProgressIndicator />;
    </StyledIndicatorContainer>

    return isProcessing ? progressIndicator : form;
}