import React from 'react';
import logo from './logo.svg';
import './App.css';
import CompanyRecordForm from './features/RegisterCompany/presentation/CompanyRecordForm';

function App() {
  return (
    <div className="App">
      <CompanyRecordForm />
    </div>
  );
}

export default App;
