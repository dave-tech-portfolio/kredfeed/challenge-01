import { useState } from "react";
import styled, { css } from "styled-components";

export const enum InputTypes {
    TEXT = 'text',
    EMAIL = 'email',
    DATE = 'date',
    NUMBER = 'number',
    FILE = 'file'
}

const StyledContainer = styled.div`
        padding: 10px;
        position: relative;
        width: 100%;
        box-sizing: border-box;
    `;
const StyledInput = styled.input`
        border: 1px solid #afbdcf;
        border-radius: 5px;
        height: 38px;
        width: 100%;
        color: #000000;
        font-size: 14px;
        padding-left: 20px;
        box-shadow: none;
        box-sizing: border-box;
        &:invalid:not(:placeholder-shown) {
            border-color: red;
        }
        ${props => props.type === InputTypes.FILE &&
        css`
            padding: 8px;
        `};

    `;

const StyledLabel = styled.label`
        font-family:arial;
        font-size:16px;
        color: #afbdcf;
        background-color: white;
        padding: 4px;
        position: absolute;
        top: 14px;
        left: 20px;
        transition:0.2s ease all; 
        -moz-transition:0.2s ease all; 
        -webkit-transition:0.2s ease all;
        pointer-events: none;
        ${StyledInput}:focus + &, ${StyledInput}:not(:placeholder-shown) + & {
            top: 0px;
            left: 6px;
            font-size: 12px;
            color:#5264AE;
        }
    `;

const StyledFileInput = styled.input`
    opacity: 0;
	overflow: hidden;
	position: absolute;
    height: 38px;
    left: 0px;
    width: 100%;
    box-sizing: border-box;
`;

const StyledFileLabel = styled.div`
    cursor: pointer;
    border: 1px solid #afbdcf;
    border-radius: 5px;
    height: 38px;
    width: 100%;
`;

export default function BrandedInput(
    { label, type, maxLength, required, name, defaultValue, onChange }:
        {
            label: string,
            type: InputTypes,
            maxLength?: number,
            required: boolean,
            name: string,
            defaultValue?: string,
            onChange?: React.ChangeEventHandler<HTMLInputElement>
        }
) {

    const [fileName, setFileName] = useState(label);

    function handleFileChanged(event: any) {
        const target = event.target;
        const value = target.value;
        setFileName(value);
        if (onChange) {
            onChange(event);
        }
    }


    const template = type === InputTypes.FILE
        ?
        <StyledContainer>
            <StyledFileInput
                required={required}
                maxLength={maxLength}
                type={type}
                name={name}
                onChange={handleFileChanged}
            />
            <StyledFileLabel><StyledLabel>{fileName}</StyledLabel></StyledFileLabel>

        </StyledContainer>
        :
        <StyledContainer>
            <StyledInput
                required={required}
                maxLength={maxLength}
                type={type}
                placeholder=" "
                name={name}
                onChange={onChange}
                defaultValue={defaultValue}
            />
            <StyledLabel>{label}</StyledLabel>
        </StyledContainer>

    return template
}