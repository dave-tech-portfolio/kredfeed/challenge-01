import { ReactNode } from "react";
import styled from "styled-components";

const StyledRow = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    height: 30px;
    width: 100%;
`;
const StyledTitle = styled.h2`
    font-size: 12px;
    font-weight: bold;
    margin-right: 10px;
`;

const StyledLine = styled.hr`
    border: 0px
    border-top: 1px solid #ccc;
    margin-top: 0;
    margin-bottom: 0;
    flex-grow: 1;
`;

const StyledContainer = styled.div`
    padding: 20px;
`;

export default function BrandedSection({ title, children }: { title: string, children: ReactNode }) {
    const template = <StyledContainer>
        <StyledRow>
            <StyledTitle>{title}</StyledTitle>
            <StyledLine />
        </StyledRow>

        {children}
    </StyledContainer>;

    return template
}