import React from 'react';
import styled, { keyframes } from 'styled-components';

interface CircleProps {
    size?: string;
}

const spinAnimation = keyframes`
  to {
    transform: rotate(360deg);
  }
`;

const Circle = styled.div<CircleProps>`
  position: relative;
  width: ${({ size }) => size || '40px'};
  height: ${({ size }) => size || '40px'};
  border: 4px solid #ccc;
  border-top-color: #07f;
  border-radius: 50%;
  animation: ${spinAnimation} 1s linear infinite;
`;

interface BrandedCircularProgressIndicatorProps {
    size?: string;
}

const BrandedCircularProgressIndicator: React.FC<BrandedCircularProgressIndicatorProps> = ({ size }) => {
    return <Circle size={size} />;
};

export default BrandedCircularProgressIndicator;
